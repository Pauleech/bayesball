# @bayesball

### A Dynamic Hierarchical Poisson Model for the Prediction of Soccer Match Outcomes

## Tags

`football forecasting`, `gambling`, `statistical modelling`, `bayesian hierarchical model`, `python`, `pystan`, `mcmc`, `nut sampling`, `data mining`

## Introduction

The problem of forecasting and analysis of the results of football matches becomes increasingly popular in the last few decades with the development of computational tools and increasing performance. Many different models have been proposed to evaluate the characteristics that lead the team to lose or win the game. Attack and defense strengths of football teams change over time due to various changes in the teams of players: from the squad to formation and tactics. 

In this project, a dynamic hierarchical Bayesian model is proposed for analyzing and predicting the results of a football match, which is assumed to come from a Poisson distribution with attack and defense abilities factors that change stochastically over time. The No-U-Turn sampler algorithm is used to solve the inverse Bayesian problem of finding such attack and defense coefficients distributions. Example validation of the model is performed on the Italian Serie A championship 2016-2017. The performance of the model is also tested on a naive betting strategy that applies to the results of the 10 top league matches in the 2016-2017 season.

## Getting Started

These instructions allow you to reproduce the project and run it on your local machine for development and testing purposes. 

### Theory

Project report is available as [pdf](bayesball.pdf) file together with problem overview and reference links.

### Prerequisites

The following software was used in this project:

* PyCharm: Python IDE for Professional Developers by JetBrains;
* Anaconda 4.4.0 Python 2.7 version;
* PyStan: The Python Interface to Stan;
* Other Python modules can be installed by `pip install`.

### Project structure

    ├── data                                    # data files
        ├── apl_12_13                           # matches results, league tables
        ├── apl_13_14
        ├── ...
    ├── src                                     # project source 
        ├── modules                             # additional modules of a common purpose
        ├── prediction                          # prediction approach
            ├── charts                          # charts files
                ├── apl_12_13                   # matches results, league tables
                ├── apl_13_14
                ├── ...
            ├── models                          # pystan models   
            ├── traces                          # sampled pystan traces
            ├── ...
    ├── config.py
    ├── main.py
    ├── ...                        

`/src` has to be marked in PyCharm as sources root folder.

### Data

Project needs season matches results, final league table for the validation and league table of the previous season for the initial teams performance values.

For the correct estimation of the initial teams performance values the relegated teams are exchanged with the promoted ones from the lower division in the league tables of the previous season.

Data is collected and prepared for 10 leagues over 5 seasons and can be downloaded [here](https://yadi.sk/d/pMcLSeYf3NsfAf).

## Running the project

The main file is `main.py` located in the root folder. 
 
It will train Bayesian model for given data and will simulate the season together with building all the charts and making predictions.

Example of the teams attack and defense abilities evolution:

<p align="center">
  <img src="/uploads/c5e8158328a0e18e7eaea2cdad0586a3/seria_a_16_17_forces_time.png" width="600px" >
</p>

Teams points simulated distributions:

> green vertical line – the median position; 

> red vertical line – a real position;

<p align="center">
  <img src="/uploads/20efeeaaaa4785f87ce5367289d01d5e/seria_a_16_17_simuls_points.png" width="600px" >
</p>


Forecasting with betting results example:

<p align="center">
  <img src="/uploads/6631ca6ff253a429174950504c95b7e4/seria_a_16_17_betting_balance_interval.png" width="600px" >
</p>

For detailed information, see [project report](bayesball.pdf).
