import os
import logging
import platform
import multiprocessing
from modules.logering import setup_logger

# number of cores
cores = multiprocessing.cpu_count()

# operating system
system = platform.system()

# folders
root_path = os.path.dirname(os.path.realpath(__file__))
data_folder = os.path.join(root_path, "data")
traces_folder = os.path.join(root_path, "src/prediction/traces")
charts_folder = os.path.join(root_path, "src/prediction/charts")
models_folder = os.path.join(root_path, "src/prediction/models")

# seed
seed = 42

# logging
log_level = logging.INFO
log_name = os.path.join(root_path, "bayesball.log")
logger = setup_logger("logger", log_name, log_level)

# training
num_samples = 3000

# leagues promotions
promotions = {
    "apl": [["champs_league"] * 4, ["europa_league"] * 3, [""] * 10, ["relegation"] * 3],
    "bra": [["libertadores"] * 6, [""] * 10, ["relegation"] * 4],
    "bundes": [["champs_league"] * 4, ["europa_league"] * 3, [""] * 8, ["play-off"] * 1, ["relegation"] * 2],
    "erediv": [["champs_league"] * 2, ["europa_league"] * 2, [""] * 11, ["play-off"] * 2, ["relegation"] * 1],
    "la_liga": [["champs_league"] * 4, ["europa_league"] * 3, [""] * 10, ["relegation"] * 3],
    "ligue_1": [["champs_league"] * 3, ["europa_league"] * 3, [""] * 11, ["relegation"] * 3],
    "primera": [["champs_league"] * 3, ["europa_league"] * 3, [""] * 10, ["relegation"] * 2],
    "rfpl": [["champs_league"] * 2, ["europa_league"] * 3, [""] * 7, ["play-off"] * 2, ["relegation"] * 2],
    "serie_a": [["champs_league"] * 3, ["europa_league"] * 3, [""] * 11, ["relegation"] * 3],
    "seria_a": [["champs_league"] * 3, ["europa_league"] * 3, [""] * 11, ["relegation"] * 3],
    "tur": [["champs_league"] * 2, ["europa_league"] * 3, [""] * 10, ["relegation"] * 3]
}
for key, value in promotions.iteritems():
    promotions[key] = [item for sublist in value for item in sublist]
