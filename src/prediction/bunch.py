import os
import config
import numpy as np
from bayesball import Bayesball
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


class Bunch:
    def __init__(self, data_folder):
        # type: (str) -> None
        """
        Initiates Bunch object.

        :rtype: None
        :param data_folder: folder with leagues folders..
        :return: None.
        """
        self.leagues = list()
        dirs = [item for item in os.listdir(data_folder)
                if os.path.isdir(os.path.join(data_folder, item))]
        for i, d in enumerate(dirs):
            self.leagues.append(Bayesball(d, os.path.join(config.data_folder, d)))

    def prepare_data(self):
        # type: () -> None
        """
        Prepares leagues seasons results data for training for all s.

        :rtype: None
        :return: None.
        """
        for league in self.leagues:
            league.prepare_data()

    def load_tables(self):
        # type: () -> None
        """
        Loads leagues tables.

        :rtype: None.
        :return: None.
        """
        for league in self.leagues:
            league.table = league.fetch_table("table")

    def check_data(self):
        # type: () -> None
        """
        Checks data correctness.

        :rtype: None
        :return: None.
        """
        for league in self.leagues:
            teams = sorted(league.table["team"].values)
            teams_prev = sorted(league.table_prev["team"].values)
            pts = league.table["pts"].values
            pts_prev = league.table_prev["pts"].values
            if not (np.array_equal(teams, teams_prev) and not np.array_equal(pts, pts_prev)):
                print league.guid
                raise Exception

    def compile_model(self):
        # type: () -> None
        """
        Compiles Stan model and sets it to all the leagues objects.

        :rtype: None.
        :return: None.
        """
        self.leagues[0].compile_model()
        for league in self.leagues[1:]:
            league.sm = self.leagues[0].sm

    def train_models(self):
        # type: () -> None
        """
        Trains leagues Stan models and saves traces variables.

        :rtype: None.
        :return: None.
        """
        for league in self.leagues:
            try:
                league.train_model()
                league.save_trace()
            except Exception as e:
                logger.error(e)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    results = Bunch(config.data_folder)
    results.prepare_data()
    results.load_tables()
    results.check_data()
    # results.compile_model()
    # results.train_models()


if __name__ == "__main__":
    main()
