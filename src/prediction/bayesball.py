import os
import math
import pickle
import config
import numpy as np
import pandas as pd
import seaborn as sns
import modules.timing as t
import modules.utils as utils
import matplotlib.pyplot as plt
from pystan import StanModel
from modules.timing import timeit
from collections import defaultdict
from modules.logering import setup_logger, logit
from dendropy.calculate.statistics import empirical_hpd, quantile
logger = setup_logger(__name__, config.log_name, config.log_level)


class Bayesball:
    @logit(logger, "league object initialing")
    def __init__(self, name, data_folder):
        # type: (str, str) -> None
        """
        Initiates Bayesball object.

        :rtype: None
        :param name: object name.
        :param data_folder: folder with matches results and tables.
        :return: None.
        """
        self.name = name
        self.data_folder = data_folder
        # region check files
        self.results_filename = os.path.join(self.data_folder, "{}_results.txt".format(self.name))
        self.table_filename = os.path.join(self.data_folder, "{}_table.txt".format(self.name))
        self.table_prev_filename = os.path.join(self.data_folder, "{}_table_prev.txt".format(self.name))
        utils.check_results_file(self.results_filename)
        utils.preprocess_results_file(self.results_filename)
        utils.check_table_file(self.table_filename)
        utils.check_table_file(self.table_prev_filename)
        # endregion
        # region variables init
        # main dataframes
        self.results = None
        self.teams = None
        self.table = None
        self.table_prev = None
        teams = pd.read_csv(self.table_filename, sep="\t")["team"].values
        self.teams_dict = utils.get_teams_abbreviations(teams)
        # class lists
        self.home_team = None
        self.away_team = None
        self.home_game = None
        self.away_game = None
        self.observed_home_goals = None
        self.observed_away_goals = None
        self.prev_att = None
        self.prev_def = None
        # class variables
        self.num_games = 0
        self.num_teams = 0
        self.num_weeks = 0
        self.team_games = 0
        # model
        self.periods = None
        self.sm = None
        self.trace = None
        self.validation = None
        self.pred_validation = None
        # simulation
        self.simuls_results = None
        self.simuls_table = None
        # endregion

    @logit(logger, "fetching results")
    def fetch_results(self):
        # type: () -> None
        """
        Fetches season results.

        :rtype: None
        :return: None.
        """
        results = pd.read_csv(self.results_filename, sep="\t")
        results["home"] = \
            results["teams"].map(lambda teams: self.teams_dict[teams.split(" - ")[0].strip()])
        results["away"] = \
            results["teams"].map(lambda teams: self.teams_dict[teams.split(" - ")[1].strip()])
        results["home_score"] = \
            results["result"].map(lambda result: int(filter(str.isdigit, result.split(":")[0].strip())))
        results["away_score"] = \
            results["result"].map(lambda result: int(filter(str.isdigit, result.split(":")[1].strip())))
        results = results.drop(["result", "teams"], axis=1)
        results = results.reindex(index=results.index[::-1])
        results.index = range(len(results))
        self.results = results

    @logit(logger, "setting teams")
    def set_teams(self):
        # type: () -> None
        """
        Sets teams DataFrame with names and indices.

        :rtype: None
        :return: None.
        """
        teams = sorted(self.results["home"].unique())
        teams = pd.DataFrame(teams, columns=["team"])
        teams["i"] = teams.index
        self.teams = teams

    @logit(logger, "processing results")
    def process_results(self):
        # type: () -> None
        """
        Processes season results and sets class variables for model training.

        :rtype: None
        :return: None.
        """
        self.results = pd.merge(self.results, self.teams, left_on="home", right_on="team", how="left")
        self.results = self.results.rename(columns={"i": "i_home"}).drop("team", 1)
        self.results = pd.merge(self.results, self.teams, left_on="away", right_on="team", how="left")
        self.results = self.results.rename(columns={"i": "i_away"}).drop("team", 1)
        # region variables set
        self.num_games = len(self.results)
        self.observed_home_goals = self.results["home_score"].values
        self.observed_away_goals = self.results["away_score"].values
        self.home_team = self.results["i_home"].values
        self.away_team = self.results["i_away"].values
        self.num_teams = len(self.teams)
        self.team_games = self.num_teams * 2 - 2
        self.num_weeks = self.num_games / (self.num_teams / 2)
        # endregion
        freq_dict = defaultdict(int)
        game_home = np.zeros(self.num_games)
        game_away = np.zeros(self.num_games)
        for i in range(self.num_games):
            home, away = self.results["home"].iloc[i], self.results["away"].iloc[i]
            freq_dict[home] += 1
            freq_dict[away] += 1
            game_home[i] = freq_dict[home] - 1
            game_away[i] = freq_dict[away] - 1
        self.results["home_game"] = pd.Series(game_home.astype(int), index=self.results.index)
        self.results["away_game"] = pd.Series(game_away.astype(int), index=self.results.index)
        self.home_game = self.results["home_game"].values
        self.away_game = self.results["away_game"].values

    @logit(logger, "setting periods")
    def set_periods(self):
        # type: () -> None
        """
        Sets betting periods regarding playing rounds.

        :rtype: None
        :return: None.
        """
        rounds = self.results["round"].values
        cur = rounds[0]
        self.periods = [[cur], ]
        for i in range(1, len(rounds)):
            if rounds[i] == cur:
                self.periods[-1].append(rounds[i])
            else:
                cur = rounds[i]
                self.periods.append([cur])
        self.periods = [len(v) for v in self.periods]
        self.periods = np.cumsum(self.periods)

    @logit(logger, "fetching table")
    def fetch_table(self, fileguid):
        # type: (str) -> DataFrame
        """
        Fetches current season results.

        :rtype: DataFrame
        :param fileguid: table or table_prev.
        :return: table.
        """
        for key in config.promotions.keys():
            if key in self.name:
                leg = key
        table = pd.read_csv(self.table_filename if fileguid == "table" else self.table_prev_filename, sep="\t")
        table["pos"] = table["pos"].map(lambda x: int(x))
        table["GF"] = table["goals"].map(lambda result: int(result.split(":")[0].strip()))
        table["GA"] = table["goals"].map(lambda result: int(result.split(":")[1].strip()))
        table["GD"] = table["GF"] - table["GA"]
        table.drop("goals", axis=1, inplace=True)
        table["QR"] = table["pos"].map(lambda x: config.promotions[leg][x - 1])
        table["team"] = table["team"].apply(lambda x: self.teams_dict[x.strip()])
        return table

    @logit(logger, "estimating initial parameters")
    def estimate_initial_parameters(self):
        # type: () -> None
        """
        Fetches previous season results to estimate initial att and def parameters.

        :rtype: None
        :return: None.
        """
        self.table_prev = self.fetch_table("table_prev")
        maxx, minx = max(self.table_prev["GF"].values), min(self.table_prev["GF"].values)
        self.table_prev["prev_att"] = self.table_prev["GF"].apply(
            lambda x: (2.0 * x / (maxx - minx) - 1.0 * (maxx + minx) / (maxx - minx)))
        maxx, minx = max(self.table_prev["GA"].values), min(self.table_prev["GA"].values)
        self.table_prev["prev_def"] = self.table_prev["GA"].apply(
            lambda x: (2.0 * x / (maxx - minx) - 1.0 * (maxx + minx) / (maxx - minx)))
        self.table_prev = self.table_prev.sort_values(by="team")
        self.table_prev.index = range(len(self.table_prev))
        self.prev_att = self.table_prev["prev_att"].values
        self.prev_def = self.table_prev["prev_def"].values

    @logit(logger, "data preparing")
    def prepare_data(self):
        # type: () -> None
        """
        Prepares season results data for training.

        :rtype: None
        :return: None.
        """
        self.fetch_results()
        self.set_teams()
        self.process_results()
        self.set_periods()
        self.estimate_initial_parameters()

    @timeit(logger, "model compiling", title=True)
    def compile_model(self, models_folder):
        # type: (str) -> None
        """
        Compiles Stan model.

        :rtype: None.
        :param models_folder: folder with Stan models.
        :return: None.
        """
        self.sm = StanModel(os.path.join(models_folder, "bayesball.stan"))

    @timeit(logger, "model training", title=True)
    def train_model(self):
        # type: () -> None
        """
        Trains Stan model and fills trace variable in.

        :rtype: None
        :return: None.
        """
        num_samples = config.num_samples
        rounds = self.team_games
        self.trace = dict()
        self.trace["att"] = np.zeros((num_samples, rounds, self.num_teams,))
        self.trace["def"] = np.zeros((num_samples, rounds, self.num_teams,))
        self.trace["home"] = np.zeros((num_samples, len(self.periods),))
        for i, p in enumerate(self.periods):
            bayes_dat = {"nteams": self.num_teams,
                         "ngames": p,
                         "nweeks": max(np.concatenate((self.home_game[0:p] + 1, self.away_game[0:p] + 1))),
                         "home_team": self.home_team[0:p] + 1,
                         "away_team": self.away_team[0:p] + 1,
                         "home_week": self.home_game[0:p] + 1,
                         "away_week": self.away_game[0:p] + 1,
                         "prev_att": self.prev_att[:],
                         "prev_def": self.prev_def[:],
                         "home_goals": self.observed_home_goals[0:p],
                         "away_goals": self.observed_away_goals[0:p]}
            with t.elapsed_timer(logger, "fitting inference for Stan model, games = {}".format(p)):
                fit = self.sm.sampling(data=bayes_dat, iter=(num_samples / 2))
                trace = fit.extract(permuted=True)
            p_prev = 0
            if i > 0:
                p_prev = self.periods[i - 1]
            for g in range(p_prev, p):
                self.trace["att"][:, self.home_game[g], self.home_team[g]] = \
                    trace["att"][:, self.home_game[g], self.home_team[g]]
                self.trace["att"][:, self.away_game[g], self.away_team[g]] = \
                    trace["att"][:, self.away_game[g], self.away_team[g]]
                self.trace["def"][:, self.home_game[g], self.home_team[g]] = \
                    trace["def"][:, self.home_game[g], self.home_team[g]]
                self.trace["def"][:, self.away_game[g], self.away_team[g]] = \
                    trace["def"][:, self.away_game[g], self.away_team[g]]
            self.trace["home"][:, i] = trace["home"]

    @logit(logger, "trace saving")
    def save_trace(self, traces_folder):
        # type: (str) -> None
        """
        Saves trace variable in a pickle file.

        :rtype: None
        :param traces_folder: folder with traces.
        :return: None.
        """
        with open(os.path.join(traces_folder, "{}_trace.pickle".format(self.name)), "wb") as handle:
            pickle.dump(self.trace, handle, protocol=pickle.HIGHEST_PROTOCOL)

    @logit(logger, "trace loading")
    def load_trace(self, traces_folder):
        # type: (str) -> None
        """
        Loads trace variable from a pickle file.

        :rtype: None
        :param traces_folder: folder with traces.
        :return: None.
        """
        with open(os.path.join(traces_folder, "{}_trace.pickle".format(self.name)), "rb") as handle:
            self.trace = pickle.load(handle)

    @logit(logger, "forces calculating")
    def calculate_forces(self):
        # type: () -> None
        """
        Calculates mean forces from a trace for the first and last season week.

        :rtype: None
        :return: None.
        """
        d = defaultdict(float)
        d["first_att"] = self.trace["att"][:, 0].mean(axis=0)
        d["first_def"] = self.trace["def"][:, 0].mean(axis=0)
        d["last_att"] = self.trace["att"][:, self.team_games - 1].mean(axis=0)
        d["last_def"] = self.trace["def"][:, self.team_games - 1].mean(axis=0)
        df_avg = pd.DataFrame(d, index=self.teams["team"].values)
        self.table = pd.merge(df_avg, self.table, left_index=True, right_on="team", how="left")

    @logit(logger, "hpd calculating")
    def calculate_hpd(self):
        # type: () -> None
        """
        Calculates HPD from a trace for the first and last season week.

        :rtype: None
        :return: None.
        """
        forces = ["att", "def"]
        states = {"first": 0, "last": self.team_games - 1}
        for force in forces:
            for key, value in states.iteritems():
                comb = "{}_{}_".format(key, force)
                d = map(empirical_hpd, self.trace[force][:, value].T)
                med_d = map(quantile, self.trace[force][:, value].T, [0.5] * self.num_teams)
                df_hpd1 = pd.DataFrame(d, columns=["{}hpd_low".format(comb), "{}hpd_high".format(comb)],
                                       index=self.teams["team"].values)
                df_hpd2 = pd.DataFrame(med_d, columns=["{}hpd_median".format(comb)],
                                       index=self.teams["team"].values)
                hpd = pd.concat([df_hpd1, df_hpd2], axis=1)
                hpd["{}relative_lower".format(comb)] = \
                    hpd["{}hpd_median".format(comb)] - hpd["{}hpd_low".format(comb)]
                hpd["{}relative_upper".format(comb)] = \
                    hpd["{}hpd_high".format(comb)] - hpd["{}hpd_median".format(comb)]
                self.table = pd.merge(hpd, self.table, left_index=True, right_on="team", how="left")

    @logit(logger, "table preparing")
    def prepare_table(self):
        # type: () -> None
        """
        Prepares table data with posterior trace data for charts.

        :rtype: None
        :return: None.
        """
        self.table = self.fetch_table("table")
        self.calculate_forces()
        self.calculate_hpd()

    def get_period(self, game_num):
        # type: (int) -> int
        """
        Returns current period for given game.

        :rtype: int
        :param game_num: game number.
        :return: current period.
        """
        per = 0
        for index, p in enumerate(self.periods):
            if game_num < p:
                per = index
                break
        return per

    def simulate_season(self):
        # type: () -> DataFrame
        """
        Simulates season results for all the games over the season.

        :rtype: DataFrame
        :return: simulated season results.
        """
        num_samples = self.trace["att"].shape[0]
        draw = np.random.randint(0, num_samples)
        season = self.results.copy()
        season["att_home"] = pd.Series(np.zeros(self.num_games), index=season.index)
        season["att_away"] = pd.Series(np.zeros(self.num_games), index=season.index)
        season["def_home"] = pd.Series(np.zeros(self.num_games), index=season.index)
        season["def_away"] = pd.Series(np.zeros(self.num_games), index=season.index)
        season["home_coef"] = pd.Series(np.zeros(self.num_games), index=season.index)
        for i in range(self.num_games):
            per = self.get_period(i)
            i_home, i_away, home_game, away_game = \
                season["i_home"].iloc[i], season["i_away"].iloc[i], \
                season["home_game"].iloc[i], season["away_game"].iloc[i]
            season.at[i, "att_home"] = self.trace["att"][draw, home_game, i_home]
            season.at[i, "att_away"] = self.trace["att"][draw, away_game, i_away]
            season.at[i, "def_home"] = self.trace["def"][draw, home_game, i_home]
            season.at[i, "def_away"] = self.trace["def"][draw, away_game, i_away]
            season.at[i, "home_coef"] = self.trace["home"][draw, per]
        season["home_theta"] = season.apply(lambda x: math.exp(x["home_coef"] + x["att_home"] + x["def_away"]),
                                            axis=1)
        season["away_theta"] = season.apply(lambda x: math.exp(x["att_away"] + x["def_home"]), axis=1)
        season["home_goals"] = season.apply(lambda x: np.random.poisson(x["home_theta"]), axis=1)
        season["away_goals"] = season.apply(lambda x: np.random.poisson(x["away_theta"]), axis=1)
        season["home_outcome"] = season.apply(lambda x: "win" if x["home_goals"] > x["away_goals"]
        else "loss" if x["home_goals"] < x["away_goals"] else "draw", axis=1)
        season["away_outcome"] = season.apply(lambda x: "win" if x["home_goals"] < x["away_goals"]
        else "loss" if x["home_goals"] > x["away_goals"] else "draw", axis=1)
        season = season.join(pd.get_dummies(season.home_outcome, prefix="home"))
        season = season.join(pd.get_dummies(season.away_outcome, prefix="away"))
        for column in ["home_win", "home_draw", "home_loss", "away_win", "away_loss", "away_draw"]:
            if column not in season.columns.values:
                season[column] = 0.0
        return season

    def create_season_table(self, season):
        # type: (DataFrame) -> DataFrame
        """
        Creates season table regarding season results DataFrame.

        :rtype: DataFrame
        :param season: season results.
        :return: season table.
        """
        g = season.groupby("i_home")
        home = pd.DataFrame({"home_goals": g.home_goals.sum(),
                             "home_goals_against": g.away_goals.sum(),
                             "home_wins": g.home_win.sum(),
                             "home_draws": g.home_draw.sum(),
                             "home_losses": g.home_loss.sum()
                             })
        g = season.groupby("i_away")
        away = pd.DataFrame({"away_goals": g.away_goals.sum(),
                             "away_goals_against": g.home_goals.sum(),
                             "away_wins": g.away_win.sum(),
                             "away_draws": g.away_draw.sum(),
                             "away_losses": g.away_loss.sum()
                             })
        df = home.join(away)
        df["wins"] = df.home_wins + df.away_wins
        df["draws"] = df.home_draws + df.away_draws
        df["losses"] = df.home_losses + df.away_losses
        df["points"] = df.wins * 3 + df.draws
        df["gf"] = df.home_goals + df.away_goals
        df["ga"] = df.home_goals_against + df.away_goals_against
        df["gd"] = df.gf - df.ga
        df = pd.merge(self.teams, df, left_on="i", right_index=True)
        df = df.sort_values(by="points", ascending=False)
        df = df.reset_index()
        df["position"] = (df.index + 1).astype(int)
        df["champion"] = (df.position == 1).astype(int)
        return df

    @timeit(logger, "seasons simulating", title=True)
    def simulate_seasons(self, n=100):
        # type: (int) -> None
        """
        Simulates seasons results n times.

        :rtype: None
        :param n: number of simulations.
        :return: simulated seasons cumulative DataFrame.
        """
        table_dfs = []
        season_dfs = []
        for i in range(n):
            season = self.simulate_season()
            table = self.create_season_table(season)
            table["iteration"] = i
            table_dfs.append(table)
            season_dfs.append(season)
        self.simuls_table = pd.concat(table_dfs, ignore_index=True)
        self.simuls_results = pd.concat(season_dfs, ignore_index=True)

    @logit(logger, "validating model")
    def validate_model(self):
        # type: () -> None
        """
        Validates model for all the games.

        :rtype: None
        :return: None.
        """
        self.results["true_outcome"] = self.results.apply(
            lambda x: 1 if x["home_score"] > x["away_score"]
            else -1 if x["home_score"] < x["away_score"]
            else 0, axis=1)
        self.simuls_results["sim_outcome"] = self.simuls_results.apply(
            lambda x: 1 if x["home_goals"] > x["away_goals"]
            else -1 if x["home_goals"] < x["away_goals"]
            else 0, axis=1)
        self.results["true_diff"] = self.results["home_score"] - self.results["away_score"]
        self.simuls_results["sim_diff"] = self.simuls_results["home_goals"] - self.simuls_results["away_goals"]
        g = self.simuls_results.groupby(["i_home", "i_away"])
        hdis = pd.DataFrame({"home_score_025": g["home_goals"].quantile(.025),
                             "home_score_975": g["home_goals"].quantile(.975),
                             "home_score_25": g["home_goals"].quantile(.25),
                             "home_score_75": g["home_goals"].quantile(.75),
                             "away_score_025": g["away_goals"].quantile(.025),
                             "away_score_975": g["away_goals"].quantile(.975),
                             "away_score_25": g["away_goals"].quantile(.25),
                             "away_score_75": g["away_goals"].quantile(.75),
                             "diff_025": g["sim_diff"].quantile(.025),
                             "diff_975": g["sim_diff"].quantile(.975),
                             "diff_25": g["sim_diff"].quantile(.25),
                             "diff_75": g["sim_diff"].quantile(.75),
                             "outcome_median": g["sim_outcome"].agg(lambda x: x.value_counts().index[0])
                             })
        hdis = pd.merge(hdis, self.results, left_index=True, right_on=["i_home", "i_away"])
        hdis["home_in_95"] = hdis.apply(lambda x: 1
        if x["home_score_025"] <= x["home_score"] <= x["home_score_975"] else 0, axis=1)
        hdis["home_in_50"] = hdis.apply(lambda x: 1
        if x["home_score_25"] <= x["home_score"] <= x["home_score_75"] else 0, axis=1)
        hdis["away_in_95"] = hdis.apply(lambda x: 1
        if x["away_score_025"] <= x["away_score"] <= x["away_score_975"] else 0, axis=1)
        hdis["away_in_50"] = hdis.apply(lambda x: 1
        if x["away_score_25"] <= x["away_score"] <= x["away_score_75"] else 0, axis=1)
        hdis["diff_in_95"] = hdis.apply(lambda x: 1
        if x["diff_025"] <= x["true_diff"] <= x["diff_975"] else 0, axis=1)
        hdis["diff_in_50"] = hdis.apply(lambda x: 1
        if x["diff_25"] <= x["true_diff"] <= x["diff_75"] else 0, axis=1)
        hdis["outcome_coin"] = hdis.apply(lambda x: 1
        if x["true_outcome"] == x["outcome_median"] else 0, axis=1)

        self.validation = dict()
        self.validation["home_in_95"] = sum(hdis["home_in_95"] / float(len(hdis)))
        self.validation["home_in_50"] = sum(hdis["home_in_50"] / float(len(hdis)))
        self.validation["away_in_95"] = sum(hdis["away_in_95"] / float(len(hdis)))
        self.validation["away_in_50"] = sum(hdis["away_in_50"] / float(len(hdis)))
        self.validation["diff_in_95"] = sum(hdis["diff_in_95"] / float(len(hdis)))
        self.validation["diff_in_50"] = sum(hdis["diff_in_50"] / float(len(hdis)))
        self.validation["accuracy"] = sum(hdis["outcome_coin"] / float(len(hdis)))

    @logit(logger, "building forces chart")
    def build_forces_chart(self, charts_folder):
        # type: (str) -> None
        """
        Builds forces charts for the first and the last week.

        :rtype: None
        :param charts_folder: folder with charts.
        :return: None.
        """
        leg = 0
        for key in config.promotions.keys():
            if key in self.name:
                leg = key
        promotions = config.promotions[leg]
        states = ["first", "last"]
        for state in states:
            fig, ax = plt.subplots(figsize=(8, 6))
            for outcome in set(promotions):
                ax.plot(self.table["{}_att".format(state)][self.table["QR"] == outcome],
                        self.table["{}_def".format(state)][self.table["QR"] == outcome], "o", label=outcome)
            for label, x, y in zip(self.table["team"].values, self.table["{}_att".format(state)].values,
                                   self.table["{}_def".format(state)].values):
                ax.annotate(label, xy=(x, y), xytext=(-5, 5), textcoords="offset points")
            ax.set_title("Attack vs Defence {} Week Effect: {}".format(state, self.name))
            ax.set_xlabel("att effect")
            ax.set_ylabel("def effect")
            ax.set_xlim([min(self.table["{}_att".format(state)]) - 0.1,
                         max(self.table["{}_att".format(state)]) + 0.1])
            ax.set_ylim([min(self.table["{}_def".format(state)]) - 0.1,
                         max(self.table["{}_def".format(state)]) + 0.1])
            ax.legend()
            filename = os.path.join(charts_folder, "forces_{}.png".format(state))
            fig.savefig(filename, dpi=200, bbox_inches="tight")

    @logit(logger, "building hpd chart")
    def build_hpd_charts(self, charts_folder):
        # type: (str) -> None
        """
        Builds hpd forces charts for the first and the last week.

        :rtype: None
        :param charts_folder: folder with charts.
        :return: None.
        """
        states = ["first", "last"]
        forces = ["att", "def"]
        colors = ["steelblue", "seagreen"]
        for force in forces:
            ymin = []
            ymax = []
            for state in states:
                comb = "{}_{}_".format(state, force)
                ymin.append(min(self.table["{}hpd_low".format(comb)].values))
                ymax.append(max(self.table["{}hpd_high".format(comb)].values))
            for state in states:
                table = self.table.copy()
                comb = "{}_{}_".format(state, force)
                table = table.sort_values(by="{}hpd_median".format(comb))
                table = table.reset_index()
                table["x"] = table.index + .5
                fig, axs = plt.subplots(figsize=(10, 4))
                axs.errorbar(table["x"], table["{}hpd_median".format(comb)],
                             yerr=(table[["{}relative_lower".format(comb),
                                          "{}relative_upper".format(comb)]].values).T,
                             fmt="o", ecolor=colors[forces.index(force)], color=colors[forces.index(force)])
                axs.set_title("HPD of {} {} Week, By Team: {}".format(force, state, self.name))
                axs.set_xlabel("team")
                axs.set_xlim(0, self.num_teams)
                axs.set_ylim(min(ymin) - 0.2, max(ymax) + 0.2)
                axs.set_ylabel("posterior {}".format(force))
                axs.set_xticks(table.index + .5)
                axs.set_xticklabels(table["team"].values, rotation=45)
                filename = os.path.join(charts_folder, "hpd_{}_{}.png".format(force, state))
                fig.savefig(filename, dpi=200, bbox_inches="tight")

    @logit(logger, "building forces time chart")
    def build_forces_time_chart(self, charts_folder):
        # type: (str) -> None
        """
        Builds forces charts for all the teams over the season.

        :rtype: None
        :param charts_folder: folder with charts.
        :return: None.
        """
        cols = 2
        if self.num_teams % 4 == 0:
            cols = 4
        elif self.num_teams % 3 == 0:
            cols = 3
        fig, axes = plt.subplots(nrows=self.num_teams / cols, ncols=cols, figsize=(15, 12))
        ymin_att = min([min(self.trace["att"][:, :, i].mean(axis=0)) for i in range(0, self.num_teams)])
        ymax_att = max([max(self.trace["att"][:, :, i].mean(axis=0)) for i in range(0, self.num_teams)])
        ymin_def = min([min(self.trace["def"][:, :, i].mean(axis=0)) for i in range(0, self.num_teams)])
        ymax_def = max([max(self.trace["def"][:, :, i].mean(axis=0)) for i in range(0, self.num_teams)])
        for i, ax in enumerate(axes.flatten()):
            ax.plot(self.trace["att"][:, :, i].mean(axis=0), label="att")
            ax.plot(self.trace["def"][:, :, i].mean(axis=0), label="def")
            ax.set_xlim([0, self.num_weeks])
            ax.set_ylim([min(ymin_att, ymin_def) - 0.2, max(ymax_att, ymax_def) + 0.2])
            ax.set_title(self.table["team"].iloc[i])
            ax.legend()
        fig.suptitle("Teams Forces Over the Season: {}".format(self.name), fontsize=18)
        fig.text(0.5, 0.07, "week", ha="center", fontsize=16)
        fig.text(0.07, 0.5, "forces", va="center", rotation="vertical", fontsize=16)
        fig.subplots_adjust(hspace=0.5)
        filename = os.path.join(charts_folder, "forces_time.png")
        fig.savefig(filename, dpi=200, bbox_inches="tight")

    @logit(logger, "building points chart")
    def build_points_chart(self, charts_folder):
        # type: (str) -> None
        """
        Builds posterior simulation points charts for all the teams.

        :rtype: None
        :param charts_folder: folder with charts.
        :return: None.
        """
        cols = 2
        if self.num_teams % 4 == 0:
            cols = 4
        elif self.num_teams % 3 == 0:
            cols = 3
        fig, axes = plt.subplots(nrows=self.num_teams / cols, ncols=cols, figsize=(15, 12))
        xmin = min(self.simuls_table["points"].values)
        xmax = max(self.simuls_table["points"].values)
        ymax = max([max(np.unique(self.simuls_table["points"][self.simuls_table["team"] == team],
                                  return_counts=True)[1])
                    for team in self.teams["team"].values])
        for i, ax in enumerate(axes.flatten()):
            team = self.teams["team"].values[i]
            points = self.simuls_table["points"][self.simuls_table["team"] == team]
            unique, counts = np.unique(points, return_counts=True)
            counts_dict = dict(zip(unique, counts))
            median = points.median()
            real = self.table["pts"][self.table["team"] == team].median()
            ax.set_title(team)
            ax.hist(points, bins=len(counts_dict.keys()), label=None)
            ax.plot([median, median], [0, ymax + 1], label="M: {}".format(median), alpha=0.7)
            ax.plot([real, real], [0, ymax + 1], label="R: {}".format(real), alpha=0.7)
            ax.set_xlim([xmin - 5, xmax + 60])
            ax.set_ylim([0, ymax + 1])
            ax.legend(loc="upper right")
        fig.suptitle("Teams Points, {} Simulations: {}"
                     .format(len(self.simuls_table) / self.num_teams, self.name), fontsize=18)
        fig.subplots_adjust(hspace=0.5)
        filename = os.path.join(charts_folder, "simuls_points.png")
        fig.savefig(filename, dpi=200, bbox_inches="tight")

    @logit(logger, "building positions chart")
    def build_positions_chart(self, charts_folder):
        # type: (str) -> None
        """
        Builds posterior simulation positions charts for all the teams.

        :rtype: None
        :param charts_folder: folder with charts.
        :return: None.
        """
        cols = 2
        if self.num_teams % 4 == 0:
            cols = 4
        elif self.num_teams % 3 == 0:
            cols = 3
        fig, axes = plt.subplots(nrows=self.num_teams / cols, ncols=cols, figsize=(15, 12))
        xmin = min(self.simuls_table["position"].values)
        xmax = max(self.simuls_table["position"].values)
        ymax = max([max(np.unique(self.simuls_table["position"][self.simuls_table["team"] == team],
                                  return_counts=True)[1])
                    for team in self.teams["team"].values])
        for i, ax in enumerate(axes.flatten()):
            team = self.teams["team"].values[i]
            points = self.simuls_table["position"][self.simuls_table["team"] == team]
            unique, counts = np.unique(points, return_counts=True)
            counts_dict = dict(zip(unique, counts))
            median = points.median()
            real = self.table["pos"][self.table["team"] == team].median()
            ax.set_title(team)
            ax.hist(points, bins=len(counts_dict.keys()), label=None)
            ax.plot([median, median], [0, ymax + 1], label="M: {}".format(median), alpha=0.7)
            ax.plot([real, real], [0, ymax + 1], label="R: {}".format(real), alpha=0.7)
            ax.set_xlim([xmin, xmax])
            ax.set_ylim([0, ymax + 1])
            ax.legend(loc="upper right", bbox_to_anchor=(1.6, 1), fancybox=True, shadow=True, frameon=True)
        fig.suptitle("Teams Positions, {} Simulations: {}"
                     .format(len(self.simuls_table) / self.num_teams, self.name), fontsize=18)
        fig.subplots_adjust(hspace=0.5)
        fig.subplots_adjust(wspace=0.8)
        filename = os.path.join(charts_folder, "simuls_positions.png")
        fig.savefig(filename, dpi=200, bbox_inches="tight")

    @logit(logger, "building goals chart")
    def build_goals_charts(self, charts_folder):
        # type: (str) -> None
        """
        Builds posterior simulation goals charts for all the teams.

        :rtype: None
        :param charts_folder: folder with charts.
        :return: None.
        """
        drs = ["gf", "ga"]
        for dr in drs:
            g = self.simuls_table.groupby("team")
            hdis = pd.DataFrame({"points_lower": g["points"].quantile(.025),
                                 "points_upper": g["points"].quantile(.975),
                                 "goals_lower": g[dr].quantile(.025),
                                 "goals_median": g[dr].median(),
                                 "goals_upper": g[dr].quantile(.975),
                                 })
            hdis = pd.merge(hdis, self.table, left_index=True, right_on="team")
            column_order = ["team", "points_lower", "pts", "points_upper",
                            "goals_lower", "GF", "goals_median", "goals_upper", "GA"]
            hdis = hdis[column_order]
            hdis["relative_goals_upper"] = hdis["goals_upper"] - hdis["goals_median"]
            hdis["relative_goals_lower"] = hdis["goals_median"] - hdis["goals_lower"]
            hdis = hdis.sort_values(by=dr.upper())
            hdis = hdis.reset_index()
            hdis["x"] = hdis.index + .5
            fig, axs = plt.subplots(figsize=(10, 6))
            axs.scatter(hdis["x"], hdis[dr.upper()], c=sns.palettes.color_palette()[4], zorder=10,
                        label="Real {}".format(dr.upper()))
            axs.errorbar(hdis["x"], hdis["goals_median"],
                         yerr=hdis[["relative_goals_lower", "relative_goals_upper"]].values.T,
                         fmt="s", c=sns.palettes.color_palette()[5], label="Simulations")
            axs.set_title("{} and 95% Interval, {} Simulations: {}".
                          format(dr.upper(), len(self.simuls_table) / self.num_teams, self.name))
            axs.set_xlabel("team")
            axs.set_ylabel(dr.upper())
            axs.set_xlim(0, 20)
            axs.legend()
            _ = axs.set_xticks(hdis.index + .5)
            _ = axs.set_xticklabels(hdis["team"].values, rotation=45)
            filename = os.path.join(charts_folder, "simuls_goals_{}.png".format(dr))
            fig.savefig(filename, dpi=200, bbox_inches="tight")

    @timeit(logger, "simulating bets", title=True)
    def simulate_bets(self, active=100., bet=1., upper_odd=4.0, start=0):
        # type: (float, float, float, int) -> (DataFrame, dict)
        """
        Simulates season results and makes bets.

        :rtype: DataFrame, float, dict
        :param active: initial active balance.
        :param bet: betting sum for one match.
        :param upper_odd: upper bound of odds variations.
        :param start: starting period to bet.
        :return: simulated DataFrame, model params.
        """
        # region inits
        params = {"active": active, "bet": bet, "upper_odd": upper_odd, "start": start}
        num_samples = self.trace["att"].shape[0]
        season = self.results.copy()
        season["active"] = active
        for col in ["result", "probs", "odds", "bets", "wager", "home_goals", "away_goals"]:
            season[col] = 0
            season[col] = season[col].astype(object)
        # endregion
        for i in range(self.periods[start], self.num_games):
            per = self.get_period(i)
            home = self.trace["home"][:, per - 1]
            home_goals = np.zeros(num_samples)
            away_goals = np.zeros(num_samples)
            i_home, i_away, home_game, away_game = \
                season["i_home"].iloc[i], season["i_away"].iloc[i], \
                season["home_game"].iloc[i], season["away_game"].iloc[i]
            # region calc results for one game
            for j in range(num_samples):
                att_home = self.trace["att"][j, home_game - 1, i_home]
                def_home = self.trace["def"][j, home_game - 1, i_home]
                att_away = self.trace["att"][j, away_game - 1, i_away]
                def_away = self.trace["def"][j, away_game - 1, i_away]
                home_theta = math.exp(home[j] + att_home + def_away)
                away_theta = math.exp(att_away + def_home)
                home_goals[j] = np.random.poisson(home_theta)
                away_goals[j] = np.random.poisson(away_theta)
            # endregion
            # region betting
            odds_1, odds_x, odds_2 = season["1"].iloc[i], season["X"].iloc[i], season["2"].iloc[i]
            home_score, away_score = season["home_score"].iloc[i], season["away_score"].iloc[i]
            result = np.array([home_score > away_score, home_score == away_score,
                               home_score < away_score]).astype(float)
            odds = np.array([odds_1, odds_x, odds_2])
            probs = np.array([(home_goals > away_goals).mean(), (home_goals == away_goals).mean(),
                              (home_goals < away_goals).mean()])
            bets = np.full(3, bet)
            bets_conf = np.zeros(3)
            bets_conf[probs.argmax()] = 1.0
            vec = np.vectorize(lambda x: 0 if x > upper_odd else 1, otypes=[np.float])
            bets = bets * vec(odds).astype(float) * bets_conf
            active -= sum(bets)
            wager = bets * odds * result
            active += sum(wager)
            # endregion
            # region df values set
            season.at[i, "result"] = result
            season.at[i, "probs"] = probs
            season.at[i, "odds"] = odds
            season.at[i, "bets"] = bets
            season.at[i, "wager"] = wager
            season.at[i, "active"] = active
            season.at[i, "home_goals"] = home_goals
            season.at[i, "away_goals"] = away_goals
            # endregion
        return season, params

    @logit(logger, "validating predictions")
    def validate_predictions(self, season, params):
        # type: (DataFrame) -> None
        """
        Validates predictions model for all the games.

        :rtype: None
        :param season: season predicted results.
        :param params: prediction params.
        :return: None.
        """
        season = season[self.periods[params["start"]]:].copy()
        season["true_outcome"] = season.apply(
            lambda x: 1 if x["home_score"] > x["away_score"]
            else -1 if x["home_score"] < x["away_score"]
            else 0, axis=1)
        season["sim_outcome"] = season.apply(
            lambda x: 1 if (x["home_goals"] > x["away_goals"]).mean()
            else -1 if (x["home_goals"] < x["away_goals"]).mean()
            else 0, axis=1)

        season["true_diff"] = season["home_score"] - season["away_score"]
        season["sim_diff"] = season["home_goals"] - season["away_goals"]

        season["home_score_025"] = season["home_goals"].apply(lambda x: np.percentile(x, 2.5))
        season["home_score_975"] = season["home_goals"].apply(lambda x: np.percentile(x, 97.5))
        season["home_score_25"] = season["home_goals"].apply(lambda x: np.percentile(x, 25))
        season["home_score_75"] = season["home_goals"].apply(lambda x: np.percentile(x, 75))
        season["away_score_025"] = season["away_goals"].apply(lambda x: np.percentile(x, 2.5))
        season["away_score_975"] = season["away_goals"].apply(lambda x: np.percentile(x, 97.5))
        season["away_score_25"] = season["away_goals"].apply(lambda x: np.percentile(x, 25))
        season["away_score_75"] = season["away_goals"].apply(lambda x: np.percentile(x, 75))
        season["diff_025"] = season["sim_diff"].apply(lambda x: np.percentile(x, 2.5))
        season["diff_975"] = season["sim_diff"].apply(lambda x: np.percentile(x, 97.5))
        season["diff_25"] = season["sim_diff"].apply(lambda x: np.percentile(x, 25))
        season["diff_75"] = season["sim_diff"].apply(lambda x: np.percentile(x, 75))
        season["outcome_median"] = season["sim_outcome"]

        season["home_in_95"] = season.apply(lambda x: 1
        if x["home_score_025"] <= x["home_score"] <= x["home_score_975"] else 0, axis=1)
        season["home_in_50"] = season.apply(lambda x: 1
        if x["home_score_25"] <= x["home_score"] <= x["home_score_75"] else 0, axis=1)
        season["away_in_95"] = season.apply(lambda x: 1
        if x["away_score_025"] <= x["away_score"] <= x["away_score_975"] else 0, axis=1)
        season["away_in_50"] = season.apply(lambda x: 1
        if x["away_score_25"] <= x["away_score"] <= x["away_score_75"] else 0, axis=1)
        season["diff_in_95"] = season.apply(lambda x: 1
        if x["diff_025"] <= x["true_diff"] <= x["diff_975"] else 0, axis=1)
        season["diff_in_50"] = season.apply(lambda x: 1
        if x["diff_25"] <= x["true_diff"] <= x["diff_75"] else 0, axis=1)
        season["outcome_coin"] = season.apply(lambda x: 1
        if x["true_outcome"] == x["outcome_median"] else 0, axis=1)

        self.pred_validation = dict()
        self.pred_validation["home_in_95"] = sum(season["home_in_95"] / float(len(season)))
        self.pred_validation["home_in_50"] = sum(season["home_in_50"] / float(len(season)))
        self.pred_validation["away_in_95"] = sum(season["away_in_95"] / float(len(season)))
        self.pred_validation["away_in_50"] = sum(season["away_in_50"] / float(len(season)))
        self.pred_validation["diff_in_95"] = sum(season["diff_in_95"] / float(len(season)))
        self.pred_validation["diff_in_50"] = sum(season["diff_in_50"] / float(len(season)))
        self.pred_validation["accuracy"] = sum(season["outcome_coin"] / float(len(season)))

    @timeit(logger, "sample simulating bets", title=True)
    def sample_simulate_bets(self, n=10, active=100., bet=1., upper_odd=4.0, start=0):
        # type: (int, float, float, float, int) -> (DataFrame, dict)
        """
        Simulates series of season results with parameters and makes bets.

        :rtype: DataFrame, float, dict.
        :param n: number of samples.
        :param active: initial active balance.
        :param bet: betting sum for one match.
        :param upper_odd: upper bound of odds variations.
        :param start: starting period to bet.
        :return: simulated DataFrame, model params.
        """
        params = {"active": active, "bet": bet, "upper_odd": upper_odd, "start": start}
        dfs = []
        for i in range(n):
            season, _ = self.simulate_bets(**params)
            dfs.append(season)
        output = pd.concat(dfs, ignore_index=True)
        return output, params

    @logit(logger, "building betting interval")
    def build_betting_interval(self, season, params, charts_folder):
        # type: (DataFrame, dict, str) -> None
        """
        Builds betting history chart for all the games of the season.

        :rtype: None
        :param season: simulated betting DataFrame.
        :param params: betting params dictionary.
        :param charts_folder: folder with charts.
        :return: None.
        """
        balance = season["active"].values
        balance = balance.reshape((len(balance) / self.num_games, self.num_games))

        lower_index = balance[:, -1].argmin()
        upper_index = balance[:, -1].argmax()

        fig, axs = plt.subplots(figsize=(10, 4))
        x = np.arange(balance.shape[1])
        est = np.mean(balance, axis=0)
        sd = np.std(balance, axis=0)
        cis = (est - sd, est + sd)
        axs.fill_between(x, cis[0], cis[1], alpha=0.2)
        axs.plot(x, est)
        axs.margins(x=0)
        axs.plot(range(0, self.num_games), balance[upper_index], label="max", color="seagreen")
        axs.plot(range(0, self.num_games), balance[lower_index], label="min", color="crimson")
        axs.plot((0, self.num_games), (params["active"], params["active"]),
                 label=None, color="black", linestyle="dashed", alpha=0.6)
        axs.set_title("Betting Balance 95% Interval, {} Simulations: {}".format(balance.shape[0], self.name))
        axs.set_xlabel("game")
        axs.set_xlim(1, self.num_games)
        axs.set_ylabel("balance")
        axs.legend(loc="upper left")
        filename = os.path.join(charts_folder, "betting_balance_interval.png")
        fig.savefig(filename, dpi=200, bbox_inches="tight")
