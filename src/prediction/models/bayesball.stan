data {
  int<lower=1> nteams;
  int<lower=1> ngames;
  int<lower=1> nweeks;
  int<lower=1> home_week[ngames];
  int<lower=1> away_week[ngames];
  int<lower=1, upper=nteams> home_team[ngames];
  int<lower=1, upper=nteams> away_team[ngames];
  int<lower=0> home_goals[ngames];
  int<lower=0> away_goals[ngames];
  row_vector[nteams] prev_att;
  row_vector[nteams] prev_def;
}

transformed data {
}

parameters {
  real home;
  real<lower=0> tau_att;
  real mu_att;
  real<lower=0> tau_def;
  real mu_def;

  matrix[nweeks, nteams] att_s;
  matrix[nweeks, nteams] def_s;
}

transformed parameters {
  matrix[nweeks, nteams] def;
  matrix[nweeks, nteams] att;
  vector[ngames] home_theta;
  vector[ngames] away_theta;

  for (w in 1:nweeks) {
    att[w] = att_s[w] - mean(att_s[w]);
    def[w] = def_s[w] - mean(def_s[w]);
  }

  for (g in 1:ngames) {
    home_theta[g] = home + att[home_week[g], home_team[g]] + def[away_week[g], away_team[g]];
    away_theta[g] = att[away_week[g], away_team[g]] + def[home_week[g], home_team[g]];
  }
}

model {
  home ~ normal(0, 1);
  mu_att ~ normal(0, 1);
  mu_def ~ normal(0, 1);
  tau_att ~ cauchy(0, 1);
  tau_def ~ cauchy(0, 1);

  att_s[1] ~ normal(prev_att, tau_att);
  def_s[1] ~ normal(prev_def, tau_def);
  for (w in 2:nweeks) {
    att_s[w] ~ normal(mu_att + att_s[w-1], tau_att);
    def_s[w] ~ normal(mu_def + def_s[w-1], tau_def);
  }

  home_goals ~ poisson_log(home_theta);
  away_goals ~ poisson_log(away_theta);
}