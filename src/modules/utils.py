import config
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


def check_results_file(filename):
    # type: (str) -> None
    """
    Checks and corrects txt file with season results.

    :rtype: None
    :param filename: results file name.
    :return: None.
    """
    with open(filename, "r") as f:
        lines = f.readlines()
    while lines[1].strip() == "":
        del lines[1]
    while lines[-1].strip() == "":
        del lines[-1]
    for i in range(1, len(lines)):
        while not lines[i][-1].isdigit():
            lines[i] = lines[i][:-1]
        lines[i] += "\n"
        lines[i] = lines[i].replace("\t\t", "\t")
    while not lines[-1][-1].isdigit():
        lines[-1] = lines[-1][:-1]
    with open(filename, "w") as f:
        f.writelines(lines)


def check_table_file(filename):
    # type: (str) -> None
    """
    Checks and corrects txt file with season tables.

    :rtype: None
    :param filename: table file name.
    :return: None.
    """
    with open(filename, "r") as f:
        lines = f.readlines()
    while lines[-1].strip() == "":
        del lines[-1]
    while lines[1].strip() == "":
        del lines[1]
    for i in range(1, len(lines)):
        while not lines[i][-1].isdigit():
            lines[i] = lines[i][:-1]
        lines[i] += "\t\n"
        lines[i] = lines[i].replace("\t\t", "\t")
    lines[-1] = lines[-1][:-1]
    with open(filename, "w") as f:
        f.writelines(lines)


def get_teams_abbreviations(teams):
    # type: (list) -> dict
    """
    Returns teams abbreviations dictionary.

    :rtype: dict
    :param teams: list of teams.
    :return: teams abbreviations dictionary.
    """
    teams_dict = dict()
    for i, team in enumerate(teams):
        while team.endswith(" "):
            team = team[:-1]
        while team.startswith(" "):
            team = team[1:]
        name = team.split()
        if len(name) == 1:
            name = name[0]
        else:
            while len(name) != 1 and len(name[0].replace(".", "")) <= 3:
                del name[0]
            name = name[0]
        z = 2
        abbr = name[:2].upper() + name[z].upper()
        while abbr in teams_dict.values():
            z += 1
            abbr = name[:2].upper() + name[z].upper()
        teams_dict[team] = abbr
    return teams_dict


def preprocess_results_file(filename):
    # type: (str) -> None
    """
    Preprocesses file with season results.

    :rtype: None
    :param filename: results file name.
    :return: None.
    """
    lines = []
    with open(filename, "r") as f:
        cur_round = None
        for i, line in enumerate(f):
            if i != 0:
                if line.find("AWA.") != -1:
                    j = line.find("AWA.")
                    if line[j - 2].isdigit():
                        line = line.replace("AWA.", "")
                    else:
                        while not line[j].isdigit():
                            line = line[:j] + line[j + 1:]
                        line = line[:j] + "0:0\t1.0\t1.0\t1.0\t" + line[j:]
                    print repr(line)
                if line.find(":") == -1:
                    cur_round = line[:line.find(".")]
                else:
                    if cur_round is None:
                        lines.append(line)
                    else:
                        lines.append("{}\t{}\n".format(line[:-1], cur_round))
            else:
                lines.append(line)
    with open(filename, "w") as f:
        for line in lines:
            f.write(line)
