import os
import config
from prediction.bayesball import Bayesball
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    name = "seria_a_16_17"
    data_folder = os.path.join(config.data_folder, name)
    charts_folder = os.path.join(config.charts_folder, name)

    league = Bayesball(name, data_folder)
    league.prepare_data()
    # league.compile_model(config.models_folder)
    # league.train_model()
    # league.save_trace(config.traces_folder)
    league.load_trace(config.traces_folder)
    league.prepare_table()
    league.simulate_seasons(100)

    # region charts
    league.build_forces_chart(charts_folder)
    league.build_hpd_charts(charts_folder)
    league.build_forces_time_chart(charts_folder)
    league.build_points_chart(charts_folder)
    league.build_positions_chart(charts_folder)
    league.build_goals_charts(charts_folder)
    # endregion
    # region validation
    league.validate_model()
    logger.info(league.validation)
    # validation predictions
    season, params = league.simulate_bets()
    league.validate_predictions(season, params)
    logger.info(league.pred_validation)
    # endregion
    season, params = league.sample_simulate_bets(n=5)
    league.build_betting_interval(season, params, charts_folder)


if __name__ == "__main__":
    main()
